import java.time.*;
import java.util.*;

// To RUN CODE:
// create directory tex/ and run
// $ javac *.java; java Calendar


public class Calendar {

    protected static PrivateInfo privateInfo = new PrivateInfoTest();
    //protected static PrivateInfo privateInfo = new PrivateInfoWasilij();
    
    protected static String widthHeaderLineInWeekly      = "1.5pt";
    protected static String widthLineBetweenDaysInWeekly = "0.8pt";
    protected static String lineWidthInsideDayInWeekly   = "0.3pt";

    protected static String lineWidthBetweenContacts     = "0.5pt";
    protected static String lineWidthInNotes             = "0.3pt";

    protected static String pathToTexDirectory = "tex";
    protected static String nameOfSubFile      = "cal.tex";
    protected static String nameOfMainFile     = "calendar.tex";

    protected static int pageCounter = 1;
    
    public static void main(String[] args){
	int year = 2021;

	String res = "";
	
	res += printFrontPage(year);
	res += newpage();

	if (pageCounter % 2 == 1){ res += newpage(); } else { res += ""; }

	res += printCalendarium(year, false, privateInfo.showDaysOfOtherMonthsInCalendarium()); // true
	res += printCalendarium(year+1, false, privateInfo.showDaysOfOtherMonthsInCalendarium());

	res += printWeekly(year, 12, true);
	res += printWeekly(year+1, 1, false);
	
	res += printMonthly(year+1);


	
	res += printContacts();

	res += printNotesPages(6);

	res += printTimeLine(year);
	
	for (int i = 1; i <= 5; i++){
	    res += newpageEnforce();
	}

	Fileinteraction.writefile(pathToTexDirectory + "/" + nameOfSubFile, res);

	Fileinteraction.writefile(pathToTexDirectory + "/" + nameOfMainFile, mainTexFile());
	
	    
    }

    protected static String printFrontPage(int year){
	String res = ""; //"\\phantom{m}\\hfill";
	res += getHeaderWeekly();
	res += "\\phantom{m}\n\n\n\\vspace{3cm}\n\n\\begin{center}\n";
	res += "\\textbf{Kalender " + year + "}\n\n\n\\vspace{2cm}\n\n\n";
	res += "\\textbf{" + privateInfo.myName() + "}\n\n\n\\vspace{2cm}\n\n\n";

	res += privateInfo.telephone() + "\n\n\n\\vspace{2cm}\n\n\n";
	res += privateInfo.contactTelephone();
	res += "\\end{center}";
	
	res += getFooterWeekly();
	return res;
    }

    protected static String getHeaderMonthly(int year){
	return "\\begin{minipage}[l]{7.5cm}{\\large \\textbf{" + year + "}} \\hfill \\\\ \n \\rule{\\textwidth}{1.5pt}\\\\\\vspace{-0.2cm} \n \\begin{tabularx}{\\textwidth}{XX}\n";
    }

    protected static String getFooterMonthly(){
	return "\\end{tabularx} \n \\end{minipage}\n";
    }

    protected static int weekOfYear(LocalDate date){
	return weekOfYear(date, date.getYear());
    }
    
    private static int weekOfYear(LocalDate date, int testYear){
	int weekOfYear;
	LocalDate trialDate = LocalDate.of(testYear, 1, 1);

	if (trialDate.getDayOfWeek().getValue() < 4){ // 1 = Mon, 7 = Sun
	    weekOfYear = 1; // ISO 8601 definition
	} else {
	    weekOfYear = 0;
	}
	
	while (trialDate.getDayOfWeek().getValue() < 7){
	    trialDate = trialDate.plusDays(1L); // Sunday
	}
	
	while (date.compareTo(trialDate) > 0){
	    trialDate = trialDate.plusDays(7L);
	    weekOfYear++;
	}

	if (weekOfYear == 0){
	    return weekOfYear(date, testYear - 1);
	} else if (weekOfYear == 53){
	    if (LocalDate.of(testYear + 1, 1, 1).getDayOfWeek().getValue() <= 4){
		weekOfYear = 1;
	    }
	}

	return weekOfYear; 
    }

    
    protected static String printMonthly(int year){
	String res = "";
	LocalDate date;
	int startingDayOfWeek;
	int[] lengthOfMonth = new int[2];
	Event event;
	
	if (pageCounter % 2 == 1){ res += newpageEnforce(); } else { res += ""; }

	
	for (int month = 1; month <= 12; month+=2){
	    res += getHeaderMonthly(year);
	    res += "\\multicolumn{1}{c}{\\textbf{" + germanNameOfMonth(month) + "}} & \\multicolumn{1}{c}{\\textbf{" + germanNameOfMonth(month+1) + "}}  \\\\ \\midrule[0.8pt] \n";

	    for (int monthIncr = 0; monthIncr <= 1; monthIncr++){
		lengthOfMonth[monthIncr] = LocalDate.of(year, month + monthIncr, 1).lengthOfMonth();
	    }
	    
	    for (int day = 1; day <= 31; day++){
		for (int monthIncr = 0; monthIncr <= 1; monthIncr++){
		    if (day <= lengthOfMonth[monthIncr]){
			date = LocalDate.of(year, month+monthIncr, day); 
		    		
			if (date.getDayOfWeek().getValue() > 5){
			    res += "\\cellcolor[HTML]{EFEFEF}\\textbf{" + day + "} ";
			} else {
			    res += day + " ";
			}

			if (date.getDayOfWeek().getValue() == 1){
			    res += "{\\tiny \\color{gray} KW " + weekOfYear(date) + "} ";
			} 

			event = gesetzlicherFeiertag(date);
			if (event != null){
			    res += "{\\tiny \\color{gray} " + event + "}";
			}
		    }

		    if (monthIncr == 0){
			res += "  &  ";
		    } else {
			res += "\\\\ \\midrule[0.3pt]\n";
		    }
		}
	    }
	    res += getFooterMonthly();
	    res += newpage();
	}

	
	return res;									
    }	
	
    protected static String printContacts(){
	String res = "";
	int lineCounter = 0;
	//int pageCounter = 0;
	//res += "\\phantom{m}\\hfill" + getHeaderWeekly() + "\n";

	if (pageCounter % 2 == 1){ res += newpageEnforce(); } else { res += ""; }

	res += getHeaderWeekly() + "\n";
	res += "\\phantom{m}\n\\phantom{m}\n";
	for (Contact c : privateInfo.getContacts()){
	    if (lineCounter > 20){
		res += getFooterWeekly();
		//pageCounter++;
		res += "\n" + newpage();
		
		res += getHeaderWeekly() + "\n";
		res += "\\phantom{m}\n\\phantom{m}\n";
		lineCounter = 0;
	    }
	    res += "\\textbf{" + c.name() + "}\\\\\n"; lineCounter++;
	    for (int i = 0; i < c.adress().length; i++){
		res += c.adress()[i] + "\\\\\n"; lineCounter++;
	    }
	    res += "\\rule{\\textwidth}{" + lineWidthBetweenContacts + "}\\\\\n";
	}
	res += getFooterWeekly();
	res += newpage();
	return res;
    }

    protected static String cross(int position){
	int size = 3;
	return "\\draw (-" + size + "pt," + (position+size) +"pt) -- (" + size + "pt," + (position-size)
	    + "pt);\n\\draw (-" + size + "pt," + (position-size) + "pt) -- (" + size + "pt," + (position+size) + "pt);\n";
    }
    
    protected static String printTimeLine(int year){
	String res = "";
	//int pageCounter = 0;
	int maxNumberOfLines = 35;
	int numberOfLines;

	int monthheight  = 133;
	String[][] monthTag = new String[][]{{"JAN", "FEB", "MAR"}, {"APR", "MAI", "JUN"}, {"JUL", "AUG", "SEP"}, {"OKT", "NOV", "DEZ"}};
	LinkedList<String> notes = privateInfo.getNotes();
	LocalDate date;
	int day, month, location;

	if (pageCounter % 2 == 1){ res += newpageEnforce(); } else { res += ""; }
	

	for (int page = 1; page <= 4; page++){
	    //if (pageCounter % 2 == 1){ res += "\\phantom{m}\\hfill"; } else { res += "\\phantom{m}"; }
	    res += getHeaderWeekly() + "\n";
	    res += "\\phantom{m}\n \\hspace{3cm}\n";

	    res += "\\begin{tikzpicture}\n";
	    res += "\\draw (0pt,0pt) -- (0pt,-" + (monthheight*3) + "pt);\n";
	    for (int i = 0; i <= 3; i++){
		res += "\\draw (-5pt,-" + (monthheight*i) + "pt) -- (10pt,-" + (monthheight*i) + "pt);\n";
	    }

	    for (int i = 0; i <= 2; i++){
		res += "\\node[rotate=90] at (-15pt,-" + (monthheight/2+i*monthheight) + "pt) {" + monthTag[page-1][2-i] + "};\n";
	    }

	    month = page*3-2;
	    day = 1;
	    while(LocalDate.of(year, month, day).getDayOfWeek().getValue() != 7){  // 1 = Mon, 7 = Sun
		day++;
	    }
	    // now in day we have a sunday!
	    
	    date = LocalDate.of(year, month, day);
	    do {
		location = -monthheight*3 + day*monthheight/31;
		res += cross(location);
		res += "\\node[rotate=90] at (10pt," + location + "pt) {" + date.getDayOfMonth() + "};\n";

		day += 7;
		date = date.plusDays(7L);
		
		//System.err.println(day + " " + date.getMonthValue() + " " + page*3 + " " + date);
	    } while ((page <= 3) && (date.getMonthValue() <= page*3) || ((page == 4) && (date.getMonthValue() > 1)));
	    
	    

	    //

	    res += "\\end{tikzpicture}\n";
	    
	    
	    /*numberOfLines = maxNumberOfLines;
	    if (page == 1){
		for (int i = 1; i <= 5; i++){ res += "\\rule{\\textwidth}{" + lineWidthInNotes + "}\\\\\n"; numberOfLines--; }
	    
		for (String s : notes){ res += s + "\\\\\n"; numberOfLines--; }
	    }
	    for (int i = 1; i <= numberOfLines; i++){ res += "\\rule{\\textwidth}{" + lineWidthInNotes + "}\\\\\n"; }
	    */
	    
	    
	    res += getFooterWeekly();
	    //pageCounter++;
	    res += newpage();
	}
	
	return res;
    }
    
    protected static String printNotesPages(int number){
	String res = "";
	//int pageCounter = 0;
	int maxNumberOfLines = 35;
	int numberOfLines;

	LinkedList<String> notes = privateInfo.getNotes();

	if (pageCounter % 2 == 1){ res += newpageEnforce(); } else { res += ""; }
	
	for (int page = 1; page <= number; page++){
	    //if (pageCounter % 2 == 0){ res += "\\phantom{m}\\hfill"; } else { res += "\\phantom{m}"; }
	    res += getHeaderWeekly() + "\n";
	    //res += "\\phantom{m}\n\\phantom{m}\n";
	    numberOfLines = maxNumberOfLines;
	    if (page == 1){
		for (int i = 1; i <= 5; i++){ res += lineInsideNotes() + "\\\\\n"; numberOfLines--; }
	    
		for (String s : notes){ res += s + "\\\\\n"; numberOfLines--; }
	    }
	    for (int i = 1; i <= numberOfLines; i++){ res += lineInsideNotes() + "\\\\\n"; }
	    res += getFooterWeekly();
	    //pageCounter++;
	    res += newpage();
	}
	return res;
    }

    protected static String lineInsideNotes(){
	return "{\\color{" + privateInfo.colorOfThinLines() + "}\\rule{\\textwidth}{" + lineWidthInNotes + "}}";
    }

    
    protected static String printWeekly(int year, int lastMonth, boolean rewind){
	LocalDate date;
	int startingDayOfWeek;
	String res = "";
	int weekOfYear;

	if (LocalDate.of(year, 1, 1).getDayOfWeek().getValue() < 4){ // 1 = Mon, 7 = Sun
	    weekOfYear = 1; // ISO 8601 definition
	} else {
	    weekOfYear = 0;
	}
	
	for (int month = 1; month <= lastMonth; month++){
	    
	    date = LocalDate.of(year, month, 1);
	    
	    startingDayOfWeek = date.getDayOfWeek().getValue();  // 1 = Mon, 7 = Sun

	    if ((rewind) && (month == 1)){
		date = date.minusDays((long) (startingDayOfWeek - 1));
	    } else {
		if (startingDayOfWeek > 1){
		    date = date.plusDays((long) (8 - startingDayOfWeek));
		}
	    }

	    if ((month == 1) && (date.getMonth().getValue() == 1) && (date.getDayOfMonth() > 1)){
		weekOfYear++;
	    }
	    
	    if (pageCounter % 2 == 1){ res += newpageEnforce(); } else { res += ""; }

	    // TODO removed usage of weekOfYear
	    
	    do {
		//res += "\\phantom{m}\\hfill";
		res += getHeaderWeekly();
		
	    	res += "{\\large \\textbf{";
		if (date.getMonth().getValue() == (date.plusDays(6L)).getMonth().getValue()){ 
		    res += germanNameOfMonth(month);
		} else {
		    res += germanNameOfMonth(date.getMonth().getValue()) + "/" + germanNameOfMonth(date.plusDays(6L).getMonth().getValue());
		}
		res += "}} \\hfill KW " + weekOfYear(date) + "\\\\\n\\rule{\\textwidth}{" + widthHeaderLineInWeekly + "}\\\\";

		
		for (int i = 1; i <= 4; i++){
		    res += printOneDayInWeekly(date, i);
		    date = date.plusDays(1L);
		}

		res += getFooterWeekly();
		res += newpage();

		res += getHeaderWeekly();
	    	res += "{\\large \\textbf{\\phantom{A}}} \\hfill " + year + "\\\\\n\\rule{\\textwidth}{" + widthHeaderLineInWeekly + "}\\\\";

		
		for (int i = 5; i <= 7; i++){
		    res += printOneDayInWeekly(date, i);
		    date = date.plusDays(1L);
		}
		res += printOneDayInWeekly(null, 0);

		res += getFooterWeekly();
		res += newpage();

		
		weekOfYear++;
		
	    } while (date.getMonth().getValue() == month);

	    
	}
	
	return res;
    }

    protected static String printOneDayInWeekly(LocalDate date, int i){
	Event event = null;
	LinkedList<Event> born;
	int numberOfCompleteLines = 0; // to please compiler
	int birthdaysPlaced = 0;
	
	String res = "\\vspace{-0.2cm}\n\n";
	res += "{\\Huge ";
	if (date != null){ res += date.getDayOfMonth(); }
	res += "} ";
	if (date != null){
	    res += germanNameOfDay(i);

	    event = gesetzlicherFeiertag(date);
	    born = birthday(date);

	    if (event != null){ 
		if (privateInfo.formatEvent() == PrivateInfo.FORMAT_EVENT_HOLIDAYS_ON_RESERVED_SPACE){
		    res += "\\hfill " + event; 
		} else if (privateInfo.formatEvent() == PrivateInfo.FORMAT_EVENT_SAVE_SPACE){
		    res += "\\hfill ";
		    if (born.size() > 0){
			res += born.getFirst();
			birthdaysPlaced = 1;
		    } 
		    res += "\\makebox(0,0)[r]{\\raisebox{31pt}{" + event + "}}";
		} else {
		    System.err.println("ERROR: No sich event formatting known!");
		}
	    } else {
		if (privateInfo.formatEvent() == PrivateInfo.FORMAT_EVENT_SAVE_SPACE){
		    if (born.size() > 1){
			res += "\\hfill " + born.get(1) + "\\makebox(0,0)[r]{\\raisebox{31pt}{" + born.get(0) + "}}";
			birthdaysPlaced = 2;
		    } else if (born.size() > 0){
			res += "\\hfill \\makebox(0,0)[r]{\\raisebox{31pt}{" + born.get(0) + "}}";
			birthdaysPlaced = 1;
		    }
		}
	    }

	    res += "\\\\\n";
	    
	    if (born.size() > 0){
		if (born.size() > 6){
		    System.err.println("ERROR: on " + date + " more than 6 people have a birthday!");
		}

		if (privateInfo.formatEvent() == PrivateInfo.FORMAT_EVENT_HOLIDAYS_ON_RESERVED_SPACE){
		    numberOfCompleteLines = 6 - born.size();
		    for (Event birth : born){
			res += "\\phantom{mm}\\rule{0.9\\textwidth-\\widthof{" + birth + "}}{0.3pt}\\hfill " + birth + "\\\\\n";
		    }
		} else if (privateInfo.formatEvent() == PrivateInfo.FORMAT_EVENT_SAVE_SPACE){
		    numberOfCompleteLines = 6 - born.size() + birthdaysPlaced; //((event == null) ? 2 : 1);
		    int counter = 0;
		    for (Event birth : born){
			if (counter >= 1 + ((event == null) ? 1 : 0)){
			    res += lineInsideDayInWeekly(birth.toString()) + "\\hfill " + birth + "\\\\\n";
			}
			counter++;
		    }
		} else {
		    System.err.println("ERROR: No sich event formatting known!");
		}
		
	    } else { numberOfCompleteLines = 6; }
	} else {
	    res += privateInfo.captionNotesInWeekly() + " \\phantom{{\\Huge 1}} \\\\\n";
	    numberOfCompleteLines = 6;
	}
	
	for (int line = 1; line <= numberOfCompleteLines; line++){
	    res += lineInsideDayInWeekly() + "\\\\\n";
	}
	res += "\\rule{\\textwidth}{" + widthLineBetweenDaysInWeekly + "}\\\\\n";
	return res;
    }

    protected static String lineInsideDayInWeekly(String string){
	return "\\phantom{mm}{\\color{" + privateInfo.colorOfThinLines() + "}\\rule{0.9\\textwidth-\\widthof{" + string + "}}{" + lineWidthInsideDayInWeekly + "}}";
    }
    
    protected static String lineInsideDayInWeekly(){
	return "\\phantom{mm}{\\color{" + privateInfo.colorOfThinLines() + "}\\rule{0.9\\textwidth}{" + lineWidthInsideDayInWeekly + "}}";
    }

    protected static Event gesetzlicherFeiertag(LocalDate date){
	LinkedList<Event> gesetzlicheFeiertage = Event.getHolidays(date.getYear());
	
	for (Event event : gesetzlicheFeiertage){
	    if (event.happensAt(date)) { return event; }
	}
	return null;
    }

    protected static LinkedList<Event> birthday(LocalDate date){
	LinkedList<Event> res = new LinkedList<Event>();
	LinkedList<Event> birthdays = privateInfo.getBirthdays();
	for (Event event : birthdays){
	    if (event.happensAt(date)) { res.add(event); }
	}
	return res;
    }

    protected static String printCalendarium(int year, boolean shiftToRight, boolean showNumbersOfOtherMonths){
	int[] orderOfMonths = new int[]{1, 7, 2, 8, 3, 9, 4, 10, 5, 11, 6, 12};
	boolean doPrint;
	
	LocalDate date;
	int startingDayOfWeek;
	String res = "";
	int weekOfYear;

	//if (shiftToRight){ res += "\\phantom{m}\\hfill"; } else { res += "\\phantom{m}\n"; }

	res += getHeaderWeekly();

	//res += "\\phantom{m}\n\n\n\\vspace{0.5cm}\n";
	res += "\n\n\\vspace{0.5cm}\n";
	res += "{\\Large " + year + "}\n\n\\vspace{0.5cm}\n\n";

	for (int monthIndex = 0; monthIndex <= 11; monthIndex++){
	    if (LocalDate.of(year, 1, 1).getDayOfWeek().getValue() < 4){ // 1 = Mon, 7 = Sun
		weekOfYear = 1; // ISO 8601 definition
	    } else {
		weekOfYear = 0;
	    }
	    for (int month = 1; month <= 12; month++){
		if (month == orderOfMonths[monthIndex]){ doPrint = true; } else { doPrint = false; }
		
		date = LocalDate.of(year, month, 1);
		
		startingDayOfWeek = date.getDayOfWeek().getValue();  // 1 = Mon, 7 = Sun
		if (month != 1){ if (startingDayOfWeek != 1){ weekOfYear--; } }
		
		date = date.minusDays((long) (startingDayOfWeek - 1));
		
		//if (month % 2 == 0){ if (doPrint){ res += "\\hfill"; } }
		if (month >= 7){ if (doPrint){ res += "\\hfill"; } }
		if (doPrint){ res += "\\begin{sideways} \\!\\!\\!\\!\\!\\!\\!\\!\\!\\!\\!\\!" + germanNameOfMonth(month) + " \\end{sideways}\n"; }
		
		if (doPrint){ res += getHeaderCalendarium(); }
		
		
		do {
		    if (doPrint){ res += "{\\color{gray}\\tiny\\textbf{" + weekOfYear(date) + "}} & "; }
		    
		    for (int i = 1; i <= 7; i++){
			if (date.getMonth().getValue() == month){ 
			    if (doPrint){ res += date.getDayOfMonth(); }
			} else {
			    if (doPrint){ res += "{\\color{" + (showNumbersOfOtherMonths ? "gray" : "white") + "}" + date.getDayOfMonth() + "}"; }
			}
			if (i != 7){ if (doPrint){ res += " &"; } }
			
			date = date.plusDays(1L);
		    }
		    if (doPrint){ res += "\\\\\n"; }
		    weekOfYear++;
		    if (weekOfYear == 53){ weekOfYear -= 52; }
		} while (date.getMonth().getValue() == month);
		
		if (doPrint){ res += getFooterCalendarium(); }
		//if (month % 2 == 0){ if (doPrint){ res += "\\hfill\n\n"; } } //else { if (doPrint){ res += "\\hfill"; } }
		if (month >= 7){ if (doPrint){ res += "\\hfill\n\n"; } }
	    }

	}

	res += getFooterWeekly();
	res += newpage();

	return res;	
    }

    protected static String getHeaderWeekly(){
	return "\\begin{minipage}[l]{7.5cm}";
    }

    protected static String getFooterWeekly(){
	return "\\end{minipage}";
    }
    
    protected static String getHeaderCalendarium(){
	//"\\setlength{\\tabcolsep}{1pt}\n\\begin{table}[h]\n\\centering\n\\begin{tabular}{cccccccc}\n" +
	return "{\\scriptsize\n\\setlength{\\tabcolsep}{0.9pt}\n\\renewcommand{\\arraystretch}{1.23}\n\\!\\!\\!\\!\\!\\!\\begin{minipage}[l]{3.1cm}\n\\begin{tabular}{cccccccc}\n" +
	    "{\\tiny KW} & "+
	    "{\\scriptsize Mo} & " +
	    "{\\scriptsize Di} & " +
	    "{\\scriptsize Mi} & " +
	    "{\\scriptsize Do} & " +
	    "{\\scriptsize Fr} & " +
	    "{\\scriptsize Sa} & " +
	    "{\\scriptsize So} \\\\\n";
    }

    protected static String getFooterCalendarium(){
	// "\\end{tabular}\n\\end{table}\n";

	return "\\end{tabular}\n\\end{minipage} }";
    }

    protected static String germanNameOfDay(int day){
	if      (day == 1){ return "Montag";     }
	else if (day == 2){ return "Dienstag";   }
	else if (day == 3){ return "Mittwoch";   }
	else if (day == 4){ return "Donnerstag"; }
	else if (day == 5){ return "Freitag";    }
	else if (day == 6){ return "Samstag";    }
	else if (day == 7){ return "Sonntag";    }
	else return "";
    }
    
    protected static String germanNameOfMonth(int month){
	if      (month == 1 ){ return "Januar";    }
	else if (month == 2 ){ return "Februar";   }
	else if (month == 3 ){ return "März";      }
	else if (month == 4 ){ return "April";     }
	else if (month == 5 ){ return "Mai";       }
	else if (month == 6 ){ return "Juni";      }
	else if (month == 7 ){ return "Juli";      }
	else if (month == 8 ){ return "August";    }
	else if (month == 9 ){ return "September"; }
	else if (month == 10){ return "Oktober";   }
	else if (month == 11){ return "November";  }
	else if (month == 12){ return "Dezember";  }
	else return "";
    }

    protected static String newpageEnforce(){
	String res = "\\phantom{m}\n\\newpage\n\n";
	if (pageCounter % 2 == 1){ res += "\\hfill"; }
	pageCounter++;
	return res;
    }

    
    protected static String newpage(){
	String res = "\n\\newpage\n\n";
	//if (pageCounter % 2 == 1){ res += "\\phantom{m}\\hfill"; } //else { res += "\\phantom{m}"; }
	if (pageCounter % 2 == 1){ res += "\\hfill"; }

	pageCounter++;
	return res;
    }

    protected static String mainTexFile(){
	String res = "";
	res += "\\documentclass{article}\n\n";
	
	res += "\\usepackage{amsmath}    % need for subequations\n";
	res += "\\usepackage{amssymb}\n";
	res += "\\usepackage{graphicx}   % need for figures\n";
	res += "\\usepackage{verbatim}   % useful for program listings\n";
	res += "\\usepackage[table,xcdraw]{xcolor}      % use if color is used in text\n";
	res += "\\usepackage{hyperref}   % use for hypertext links, including those to external documents and URLs\n";
	res += "\\usepackage{enumerate}\n";
	res += "\\usepackage{ngerman}\n";
	res += "\\usepackage{tikz}\n";
	res += "\\usepackage[utf8]{inputenc}\n";
	res += "\\usepackage{rotating}\n";
	res += "\\usepackage{tabularx}\n";
	res += "\\usepackage{calc}\n";
	res += "\\usepackage{booktabs}\n";
	res += "\\setlength{\\aboverulesep}{0pt}\n";
	res += "\\setlength{\\belowrulesep}{0pt}\n";
	
	String[] extrapackages = privateInfo.extraPreambleCommands();
	if (extrapackages != null){
	    for (int i = 0; i < extrapackages.length; i++){
		res += extrapackages[i] + "\n";
	    }
	}

	res += "\n";
	
	res += "\\textwidth=20.3cm % 16.3cm\n";
	res += "\\oddsidemargin=-1.9cm\n";
	res += "\\textheight=22cm\n";
	res += "\\topmargin=-3.5cm\n\n";
	
	res += "\\newcommand{\\ds}{\\displaystyle}\n\n";
	
	res += "\\setlength\\parindent{0pt}\n\n";
	
	res += "\\begin{document}\n\n";
	
	res += "\\input{cal}\n\n";
	
	res += "\\end{document}\n";

	return res;
    }
}
