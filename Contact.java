import java.util.*;

public class Contact {

    protected String[] adressLines;
    protected String name;
    
    public Contact(String name, String[] adressLines){
	this.name = name;
	this.adressLines = adressLines;
    }

    public String name(){ return name; }

    public String[] adress(){ return adressLines; }
    
}
