import java. time.*;
import java.util.*;

public class Event {

    protected LocalDate date;
    protected String text;

    public Event(LocalDate date, String text){
	this.date = date;
	this.text = text;
    }

    public Event(int day, int month, String text){
	this.date = LocalDate.of(0, month, day);
	this.text = text;
    }

    public Event(int day, int month, int year, String text){
	this.date = LocalDate.of(year, month, day);
	this.text = text;
    }

    public String toString(){
	return text + (date.getYear() != 0 ? " (" + date.getYear() + ")" : "");
    }

    public boolean happensAt(LocalDate otherDate){
	return ((date.getMonth().getValue() == otherDate.getMonth().getValue())
		&&
		(date.getDayOfMonth() == otherDate.getDayOfMonth()));
    }

    public static LinkedList<Event> getHolidays(int year){

	LinkedList<Event> holidays = new LinkedList<Event>();
	holidays.add(new Event( 1, 1,"Neujahr"));
	holidays.add(new Event( 6, 1,"Hl. Drei Könige"));
	holidays.add(new Event( 1, 5,"Tag der Arbeit"));
	holidays.add(new Event( 1, 8,"Nationalfeiertag (CH)"));
	holidays.add(new Event(15, 8,"Maria Himmelfahrt (BY)"));
	holidays.add(new Event( 3,10,"Dt. Einheit"));
	holidays.add(new Event( 1,11,"Allerheiligen"));
	holidays.add(new Event(25,12,"Erster Weihnachtstag"));
	holidays.add(new Event(26,12,"Zweiter Weihnachtstag"));
	    	
	if (year == 2020){
	    holidays.add(new Event(29, 3,"Beginn Sommerzeit"));
	    holidays.add(new Event(25,10,"Ende Sommerzeit"));
	    
	    holidays.add(new Event(24, 2,"Rosenmontag"));
	    holidays.add(new Event(10, 4,"Karfreitag"));
	    holidays.add(new Event(12, 4,"Ostersonntag"));
	    holidays.add(new Event(13, 4,"Ostermontag"));
	    holidays.add(new Event(20, 4,"Sechseläuten (CH)"));
	    holidays.add(new Event(21, 5,"Himmelfahrt"));
    	    holidays.add(new Event(31, 5,"Pfingstsonntag"));
	    holidays.add(new Event( 1, 6,"Pfingstmontag"));
	    holidays.add(new Event(11, 6,"Fronleichnam"));
	    holidays.add(new Event(14, 9,"Knabenschiessen (CH)"));
	    holidays.add(new Event(18,11,"Bus- und Bett-Tag"));
	    holidays.add(new Event(29,11,"Erster Advent"));
	    holidays.add(new Event(06,12,"Zweiter Advent"));
	    holidays.add(new Event(13,12,"Dritter Advent"));
	    holidays.add(new Event(20,12,"Vierter Advent"));
	    
	    return holidays;
	} else if (year == 2021){
	    holidays.add(new Event(28, 3,"Beginn Sommerzeit"));
	    holidays.add(new Event(31,10,"Ende Sommerzeit"));
	    
	    holidays.add(new Event(15, 2,"Rosenmontag"));
	    holidays.add(new Event( 2, 4,"Karfreitag"));
	    holidays.add(new Event( 4, 4,"Ostersonntag"));
	    holidays.add(new Event( 5, 4,"Ostermontag"));
	    holidays.add(new Event(19, 4,"Sechseläuten (CH)"));
	    holidays.add(new Event(13, 5,"Himmelfahrt"));
    	    holidays.add(new Event(23, 5,"Pfingstsonntag"));
	    holidays.add(new Event(24, 5,"Pfingstmontag"));
	    holidays.add(new Event( 3, 6,"Fronleichnam"));
	    holidays.add(new Event(13, 9,"Knabenschiessen (CH)"));
	    holidays.add(new Event(17,11,"Bus- und Bett-Tag"));
	    holidays.add(new Event(28,11,"Erster Advent"));
	    holidays.add(new Event(05,12,"Zweiter Advent"));
	    holidays.add(new Event(12,12,"Dritter Advent"));
	    holidays.add(new Event(19,12,"Vierter Advent"));

	    
	    return holidays;
	} else if (year == 2022){
	    holidays.add(new Event(27, 3,"Beginn Sommerzeit"));
	    holidays.add(new Event(30,10,"Ende Sommerzeit"));
	    
	    holidays.add(new Event(28, 2,"Rosenmontag"));
	    holidays.add(new Event(15, 4,"Karfreitag"));
	    holidays.add(new Event(17, 4,"Ostersonntag"));
	    holidays.add(new Event(18, 4,"Ostermontag"));
	    holidays.add(new Event(25, 4,"Sechseläuten (CH)"));
	    holidays.add(new Event(26, 5,"Himmelfahrt"));
    	    holidays.add(new Event( 5, 6,"Pfingstsonntag"));
	    holidays.add(new Event( 6, 6,"Pfingstmontag"));
	    holidays.add(new Event(16, 6,"Fronleichnam"));
	    holidays.add(new Event(12, 9,"Knabenschiessen (CH)"));
	    holidays.add(new Event(16,11,"Bus- und Bett-Tag"));
	    holidays.add(new Event(27,11,"Erster Advent"));
	    holidays.add(new Event(04,12,"Zweiter Advent"));
	    holidays.add(new Event(11,12,"Dritter Advent"));
	    holidays.add(new Event(18,12,"Vierter Advent"));

	    holidays.add(new Event( 4, 1,"Perihel 07:52"));
	    
	    return holidays;
	} else {
	    System.err.println("ERROR: No holidays found for " + year);
	    return null;
	}
    }



    /*    public static LinkedList<Event> getAstronomicalEvents(int year){

	LinkedList<Event> astro = new LinkedList<Event>();
	    	
	if (year == 2020){
	    astro.add(new Event(29, 3,"Beginn Sommerzeit"));
	    
	    return astro;
	} else if (year == 2021){
	    astro.add(new Event(28, 3,"Beginn Sommerzeit"));

	    return astro;
	} else {
	    System.err.println("ERROR: No astronomical events found for " + year);
	    return null;
	}
	} */

}
