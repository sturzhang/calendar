import java.util.*;

public abstract class PrivateInfo {

    
    public static int FORMAT_EVENT_HOLIDAYS_ON_RESERVED_SPACE = 0;
    public static int FORMAT_EVENT_SAVE_SPACE                 = 1;

    public abstract String captionNotesInWeekly();

    public abstract String colorOfThinLines();
    
    public abstract String myName();
    public abstract String telephone();
    public abstract String contactTelephone();

    public abstract LinkedList<Contact> getContacts();

    public abstract LinkedList<String> getNotes();

    public abstract LinkedList<Event> getBirthdays();

    public abstract int formatEvent();

    public abstract String[] extraPreambleCommands();

    public abstract boolean showDaysOfOtherMonthsInCalendarium();
}
