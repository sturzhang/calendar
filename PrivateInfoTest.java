import java.util.*;

public class PrivateInfoTest extends PrivateInfo {

    @Override
    public String myName(){ return "Max Mustermann"; }

    @Override
    public String telephone(){ return "+49 123 456 789"; } 

    @Override
    public String contactTelephone(){ return "+49 987 654 321"; }

    @Override
    public String captionNotesInWeekly(){ return "Notizen"; }

    @Override
    public String colorOfThinLines(){ return "black"; }

    @Override
    public LinkedList<Contact> getContacts(){
	LinkedList<Contact> res = new LinkedList<Contact>();
	
	res.add(new Contact("Some Friend", new String[]{"Nice Street. 11 23456 City Country"}));
	return res;
    }

    @Override
    public LinkedList<String> getNotes(){
	LinkedList<String> notes = new LinkedList<String>();
	
	notes.add("important note");

	return notes;
    }

    @Override
    public LinkedList<Event> getBirthdays(){
	LinkedList<Event> birthdays = new LinkedList<Event>();
	
	birthdays.add(new Event(1,1,2000,"birthday Peter"));
	birthdays.add(new Event(2,1,"birthday Anne1"));
	birthdays.add(new Event(2,1,"birthday Anne2"));
	birthdays.add(new Event(6,1,2000,"birthday Peter1"));
	birthdays.add(new Event(6,1,2000,"birthday Peter2"));
	birthdays.add(new Event(8,1,2000,"birthday Peter1"));
	birthdays.add(new Event(8,1,2000,"birthday Peter2"));
	birthdays.add(new Event(10,1,2000,"birthday Peter"));
	
	return birthdays;
    }

    @Override
    public int formatEvent(){
	return FORMAT_EVENT_SAVE_SPACE;
    }

    @Override
    public String[] extraPreambleCommands(){
	return new String[]{"\\usepackage{libertine}"};
    }

    @Override
    public boolean showDaysOfOtherMonthsInCalendarium(){
	return true;
    }

}
